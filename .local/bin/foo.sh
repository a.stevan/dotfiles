#!/usr/bin/env bash

TERM=$"$HOME/.local/share/cargo/bin/alacritty"
TERM_FLAGS="-e"
NU=$"$HOME/.local/bin/nu"

"$TERM" "$TERM_FLAGS" "$NU" \
    --commands '$env.SHELL = $nu.current-exe; tmux new-session -A -s (random uuid)'
