#!/usr/bin/env bash

TERM=$"$HOME/.local/share/cargo/bin/alacritty"
TERM_FLAGS="-e"
NU=$"$HOME/.local/bin/nu"

"$TERM" "$TERM_FLAGS" "$NU" \
    --env-config ~/.config/nushell/env.nu \
    --commands "logout.nu --lock 'gnome-screensaver-command --lock'"
